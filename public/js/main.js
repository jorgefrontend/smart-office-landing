    //-----------
    //---- NAVIGATION

    let icon = document.getElementById('icon'); // OBTENER ICONO MENU
    let nav = document.getElementById('nav'); // OBTENER NAVEGACIÓN

    icon.addEventListener('click', e =>{       
        icon.classList.toggle('iconClick')
        nav.classList.toggle('navTranslate')
    });

    nav.addEventListener('click', e =>{
       if(e.target.classList.contains('item__link')){
            icon.classList.toggle('iconClick')
            nav.classList.toggle('navTranslate')
       }
    });


    
    //-----------
    //---- SCROLL

	$('a[href*="#"]')
        .not('[href="#"]')
        .not('[href*="#tabs-"]')
        .not('[href="#0"]')
        .click(function(event) {
        
            if (
                location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
                &&
                location.hostname == this.hostname
            ) {
              
                let target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
         
                if (target.length) {
       
                    event.preventDefault();
                    $('html, body').animate({
                       
                        scrollTop: target.offset().top - 0
                    }, 500, function() {
              
                        let $target = $(target);
                        $target.focus();
                        if ($target.is(":focus")) { 
                            return false;
                        } else {
                            $target.attr('tabindex','-1'); 
                            $target.focus(); 
                        }
                    });
                }
            }
        });

//-----------
//---- ACCORDION

$(function () {
    var Accordion = function (el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.link');
        // Evento
        links.on('click', { el: this.el, multiple: this.multiple }, this.dropdown)
    }

    Accordion.prototype.dropdown = function (e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();
        $this.parent().toggleClass('open');

        if (!e.data.multiple) {
            $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
    }

    var accordion = new Accordion($('#accordion'), false);
    var accordion = new Accordion($('#accordion1'), false);
    var accordion = new Accordion($('#accordion2'), false);
    var accordion = new Accordion($('#accordion3'), false)
    var accordion = new Accordion($('#accordion4'), false)
});





 

